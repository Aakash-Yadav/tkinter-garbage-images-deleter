from os import (listdir,removedirs,remove)
from tkinter import *  
from pathlib import Path  

class Gui(object):
    def __init__(self):
        self.root = Tk()
        self.root.geometry('100x100')
        self.root.title('Delete SS')
        self.root.resizable(0,0)
        self.name_files = remove_image_files_from_local().get_image_name()
        self.button1 = Button(self.root,text=f' Total={len(self.name_files)} ',
        command= lambda :self.total_image(self.button1))
        self.button1.pack()
        self.button2 = Button(self.root,text='delete',command=self.main_gui())
        self.button2.pack()
    
    def main_gui(self):
        (remove_image_files_from_local().remove_file(self.name_files))
        return True 
    
    def total_image(self,n):
        n['text'] = 0 
        n['text']=f'{len(remove_image_files_from_local().get_image_name())}'
        return True 

class remove_image_files_from_local(object):

    def __init__(self):
        self.list_of_files = (listdir( Path.home()))
    
    def get_image_name(self,list_of_names=listdir(Path.home()),data=[]):
        if not list_of_names:
            return (sorted(data))
        else:
            x = list_of_names[-1]
            print(x,data.append(x)) if x.endswith('.png') else False 
            return self.get_image_name(list_of_names[:-1],data)
    
    def remove_file(self,name):
        if not name:
            return True 
        else:
            x = name[-1]
            remove(x)
            return self.remove_file(name[:-1])
    
    def __str__(self):
        return f'name = {self.get_image_name(self.list_of_files)}'




if __name__ == '__main__':
    Gui()
    mainloop()
