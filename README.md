# Tkinter Garbage images deleter

Using Gui Tkinter and Os module remove trash or Garbage images from home path 

### Tkinter

Tkinter is a Python binding to the Tk GUI toolkit. It is the standard Python interface to the Tk GUI toolkit, and is Python's de facto standard GUI. Tkinter is included with standard GNU/Linux, Microsoft Windows and macOS installs of Python. The name Tkinter comes from Tk interface. Tkinter was written by Fredrik Lundh. Tkinter is free software released under a Python license.


### Before execution of code

![alt text](https://xp.io/storage/1W2LXGup.png)

### After execution of code

![alt text](https://xp.io/storage/1W2Qiteu.png)

#### thank you
